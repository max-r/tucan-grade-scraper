from netrc import netrc
from bs4 import BeautifulSoup as bs4
import mechanize
from lxml import html
import pandas as pd
import re
from datetime import date

base_url = 'https://www.tucan.tu-darmstadt.de'

# login
br = mechanize.Browser()
br.open(
    "https://www.tucan.tu-darmstadt.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=STARTPAGE_DISPATCH&ARGUMENTS=-N000000000000001")
br.select_form(nr=0)
username, _, password = netrc().authenticators("www.tucan.tu-darmstadt.de")
br.form["usrname"] = username
br.form["pass"] = password
br.submit()

# go to Leistungsspiegel

br.follow_link(text_regex=u"^Prüfungen$")
br.follow_link(text_regex=u"^Leistungsspiegel$")

# find table rows in html
res_html = br.response().read()

tree = html.fromstring(res_html)

tbody = tree.xpath("//table[@class='nb list students_results']/tbody")[0]
soup = bs4(html.tostring(tbody), 'html.parser')
table_rows = soup.find_all('tr')

df = pd.DataFrame()


def squeeze_spaces(text):
    return ' '.join(text.strip().split())


# get information for each course
record = {}
for tr in table_rows:
    if len(tr.find_all('td')[0].text) > 0 \
            and tr.find_all('td')[0].text[0].isdigit() \
            and len(tr.find_all('td')[3].text) > 0 \
            and squeeze_spaces(tr.find_all('td')[0].text)[0].isdigit():

        record['id'] = squeeze_spaces(tr.find_all('td')[0].text)
        record['name'] = squeeze_spaces(tr.find_all('td')[1].text)
        record['cp'] = squeeze_spaces(tr.find_all('td')[3].text)
        record['counted_cp'] = squeeze_spaces(tr.find_all('td')[4].text)
        record['grade'] = squeeze_spaces(tr.find_all('td')[5].text)

        # get link for detail view
        link_detail = base_url + re.search('javascript: dl_popUp\(\'(.+)\',\'Ergebnisdetails.*',
                                           tr.find('a').attrs['onclick']).group(1)

        # get information from detail view
        br.open(link_detail)
        html_detail = br.response().read()
        soup_detail = bs4(html_detail, 'html.parser')
        tds = soup_detail.find('table').find_all('tr')[4].find_all('td')
        record['semester'] = ' '.join(tds[0].text.strip().split())
        record['date'] = ' '.join(tds[2].text.strip().split())

        # get link for overview
        link_overview = base_url + re.search('javascript:popUp\(\'(.+)\'\);', tds[5].find('a').attrs['href']).group(1)

        # get information from overview
        br.open(link_overview)
        html_overview = br.response().read()
        soup_overview = bs4(html_overview, 'html.parser')

        tds = soup_overview.find_all('table')[-1].find_all('tr')[1].find_all('td')[1:]

        grade_steps = ['1,0', '1,3', '1,7', '2,0', '2,3', '2,7', '3,0', '3,3', '3,7', '4,0', '5,0']
        for i, step in enumerate(grade_steps):
            record[step] = squeeze_spaces(tds[i].text) if squeeze_spaces(tds[i].text) != '---' else '0'

        print(record)
        # append data to dataframe
        df = df.append(record, ignore_index=True)

    elif 'class' in tr.attrs and 'subhead' in tr.attrs['class']:
        if 'level00' in tr.attrs['class']:
            record['course_of_study'] = squeeze_spaces(tr.find('td').text)
            record['domain'] = ''
            record['mandatory'] = True
            record['type'] = 'Default'
            record['faculty'] = ''
        elif 'level01' in tr.attrs['class']:
            record['domain'] = squeeze_spaces(tr.find('td').text)
            record['mandatory'] = True
            record['type'] = 'Default'
            record['faculty'] = ''
        elif 'level02' in tr.attrs['class']:
            record['mandatory'] = False if 'wahl' in squeeze_spaces(tr.find('td').text).lower() else True
            record['type'] = 'Default'
            record['faculty'] = ''
        elif 'level03' in tr.attrs['class']:
            record['type'] = squeeze_spaces(tr.find('td').text)
            record['faculty'] = ''
        elif 'level04' in tr.attrs['class']:
            record['faculty'] = squeeze_spaces(tr.find('td').text)

# write results to xlsx
df.to_excel(date.today().strftime("%Y-%m-%d")+"_tucan-grades.xlsx", index=False)
