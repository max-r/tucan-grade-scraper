# Tucan Grade Analyser

Extracts grades and useful course information from TUCAN for the basis of further analysis and saves it to an xlsx file.

## Installation

Install requirements:

```bash
pip install -r requirements.txt
```

## Setup

Create file '.netrc' with your username and password and place it under ~/
(Windows: C:/<User>/) 

.netrc : 
```bash
machine www.tucan.tu-darmstadt.de
login <username>
password <password>
```

## Execute
Run
```bash
python ./scrape_grades.py
```

## Info
Thanks to fhirschmann!
Used code fragments from https://github.com/fhirschmann/tucan